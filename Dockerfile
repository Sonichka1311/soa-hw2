FROM golang:1.15

RUN mkdir /hw2/
WORKDIR /hw2/

COPY go.mod go.mod
COPY cmd cmd
COPY pkg pkg

RUN mkdir pkg/output/

ENTRYPOINT ["go", "run", "./cmd/main.go"]
