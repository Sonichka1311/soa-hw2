package main

import (
	"fmt"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/avro"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/datastruct"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/file"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/json"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/msgpack"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/native"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/proto"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/xml"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/yaml"
	"log"
	"time"
)

const iters = 1

func Test(structure *datastruct.Struct, serialise func(structure *datastruct.Struct) []byte, deserialize func(data []byte, to *datastruct.Struct)) (size int, timeSer, timeDeser float64) {
	message := serialise(structure)
	size = len(message)
	file.Write(message, "pkg/output/tmp.txt")

	start := time.Now()
	for i := 0; i < iters; i++ {
		serialise(structure)
	}
	timeSer = time.Now().Sub(start).Seconds()

	start = time.Now()
	to := datastruct.Struct{}
	for i := 0; i < iters; i++ {
		deserialize(message, &to)
	}
	timeDeser = time.Now().Sub(start).Seconds()

	return size, timeSer / iters, timeDeser / iters
}

func PrintTestResult(name string, structure *datastruct.Struct, serialise func(structure *datastruct.Struct) []byte, deserialize func(data []byte, to *datastruct.Struct)) {
	log.Printf("Start testing %s...", name)
	res := FormatTestResult(Test(structure, serialise, deserialize))
	log.Printf("\nRESULTS FOR %s\n%s", name, res)

	dir := "pkg/output"
	oldPath := fmt.Sprintf("%s/tmp.txt", dir)
	newPath := fmt.Sprintf("%s/res_%s.", dir, name)
	switch name {
	case "native", "protobuf", "avro", "messagepack":
		name = "txt"
	}
	file.Rename(oldPath, fmt.Sprintf("%s%s", newPath, name))
}

func FormatTestResult(size int, timeSer, timeDeser float64) string {
	return fmt.Sprintf("OUTPUT SIZE: %d b \nAVG SERIALIZE TIME: %f s\nAVG DESERIALIZE TIME: %f s\n\n", size, timeSer, timeDeser)
}

func main() {
	log.Println("Creating the structure...")
	structure := datastruct.GetStruct()

	PrintTestResult("native", &structure, native.Serialize, native.Deserialize)
	PrintTestResult("xml", &structure, xml.Serialize, xml.Deserialize)
	PrintTestResult("json", &structure, json.Serialize, json.Deserialize)
	PrintTestResult("protobuf", &structure, proto.Serialize, proto.Deserialize)
	PrintTestResult("avro", &structure, avro.Serialize, avro.Deserialize)
	PrintTestResult("yaml", &structure, yaml.Serialize, yaml.Deserialize)
	PrintTestResult("messagepack", &structure, msgpack.Serialize, msgpack.Deserialize)
}
