module gitlab.com/Sonichka1311/soa-hw2

go 1.15

require (
	github.com/golang/protobuf v1.4.3
	github.com/hamba/avro v1.5.4
	github.com/vmihailenco/msgpack/v5 v5.2.0
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.4.0
)
