package avro

import (
	"github.com/hamba/avro"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/datastruct"
	"log"
)

var schema = `{
	    "type": "record",
	    "name": "simple",
	    "namespace": "org.hamba.avro",
	    "fields": [
	        {"name": "Int", "type": "int"},
	        {"name": "Double", "type": "double"},
			{"name": "String", "type": "string"},
			{
				"name":"Array",
				"type": {
					"type": "array",
					"items": {"name": "int", "type": "int"}
				}
			}
	    ]
	}`

func Serialize(structure *datastruct.Struct) []byte {
	schema, err := avro.Parse(schema)
	if err != nil {
		log.Fatalf("Error while serialize avro: %s", err.Error())
		return nil
	}
	res, err := avro.Marshal(schema, structure)
	if err != nil {
		log.Fatalf("Error while serialize avro: %s", err.Error())
		return nil
	}
	return res
}

func Deserialize(data []byte, to *datastruct.Struct) {
	schema, err := avro.Parse(schema)
	if err != nil {
		log.Fatalf("Error while deserialize avro: %s", err.Error())
	}
	err = avro.Unmarshal(schema, data, &to)
	if err != nil {
		log.Fatalf("Error while deserialize avro: %s", err.Error())
	}
}
