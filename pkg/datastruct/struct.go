package datastruct

type Struct struct {
	Int    int
	Double float64
	String string
	Array  []int
	//Dict   map[string]int64
}

func GetStruct() Struct {
	structure := Struct{
		Int:    100500,
		Double: 100500.100500,
		String: "this is structure",
	}
	// 100000000
	iters := 100000000
	array := make([]int, 0, iters)
	//dict := make(map[string]int, iters)
	for i := 0; i < iters; i++ {
		array = append(array, i)
		//dict[strconv.Itoa(i)] = i
	}
	structure.Array = array
	//structure.Dict = dict

	return structure
}
