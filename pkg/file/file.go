package file

import (
	"log"
	"os"
	"time"
)

func Write(what []byte, to string) {
	createFileIfNotExists(to)

	f, err := os.OpenFile(to, os.O_WRONLY, os.ModeAppend)
	if err != nil {
		log.Printf("can't create file %s\n", to)
		return
	}

	_, err = f.Write(what)
	if err != nil {
		log.Printf("cant write to file %s: %s\n", to, err.Error())
		return
	}
	f.Sync()
}

func Rename(from, to string) {
	os.Remove(to)
	os.Rename(from, to)
}

func createFileIfNotExists(name string) bool {
	if _, err := os.Stat(name); os.IsNotExist(err) {
		_, err := os.Create(name)
		i := 0
		for err != nil {
			_, err = os.Create(name)
			i++
			if i > 10 {
				break
			}
			time.Sleep(10)
		}
		return true
	}
	return false
}
