package json

import (
	"encoding/json"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/datastruct"
	"log"
)

func Serialize(structure *datastruct.Struct) []byte {
	res, err := json.Marshal(structure)
	if err != nil {
		log.Fatalf("Error while serialize JSON: %s", err.Error())
	}
	return res
}

func Deserialize(data []byte, to *datastruct.Struct) {
	err := json.Unmarshal(data, &to)
	if err != nil {
		log.Fatalf("Error while deserialize JSON: %s", err.Error())
	}
}
