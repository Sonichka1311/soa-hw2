package msgpack

import (
	"github.com/vmihailenco/msgpack/v5"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/datastruct"
	"log"
)

func Serialize(structure *datastruct.Struct) []byte {
	res, err := msgpack.Marshal(structure)
	if err != nil {
		log.Fatalf("Error while serialize MessagePack: %s", err.Error())
	}
	return res
}

func Deserialize(data []byte, to *datastruct.Struct) {
	err := msgpack.Unmarshal(data, &to)
	if err != nil {
		log.Fatalf("Error while deserialize MessagePack: %s", err.Error())
	}
}