package native

import (
	"bytes"
	"encoding/gob"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/datastruct"
	"log"
)

func Serialize(structure *datastruct.Struct) []byte {
	var encode bytes.Buffer
	encoder := gob.NewEncoder(&encode)
	err := encoder.Encode(structure)
	if err != nil {
		log.Fatalf("Error while serialize native: %s", err.Error())
	}
	return encode.Bytes()
}

func Deserialize(data []byte, to *datastruct.Struct) {
	var decode bytes.Buffer
	decode.Write(data)
	decoder := gob.NewDecoder(&decode)
	err := decoder.Decode(&to)
	if err != nil {
		log.Fatalf("Error while deserialize native: %s", err.Error())
	}
}