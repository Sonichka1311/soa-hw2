package proto

import (
	"github.com/golang/protobuf/proto"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/datastruct"
	"log"
)

func Serialize(structure *datastruct.Struct) []byte {
	str := func() interface{} { return GetPBStruct(structure) }()
	res, err := proto.Marshal(str.(proto.Message))
	if err != nil {
		log.Fatalf("Error while serialize Protobuf: %s", err.Error())
	}
	return res
}

func Deserialize(data []byte, to *datastruct.Struct) {
	str := func() interface{} { return GetPBStruct(to) }()
	tomsg := str.(proto.Message)
	err := proto.Unmarshal(data, tomsg)
	if err != nil {
		log.Fatalf("Error while deserialize Protobuf: %s", err.Error())
	}
	str = tomsg
	to = GetFromPBStruct(str.(*PBStruct))
}

func GetPBStruct(structure *datastruct.Struct) *PBStruct {
	array := make([]int64, len(structure.Array))
	for idx, item := range structure.Array {
		array[idx] = int64(item)
	}
	return &PBStruct{
		Int:     int64(structure.Int),
		Double:  structure.Double,
		String_: structure.String,
		//Array:   structure.Array,
		Array:   array,
		//Dict: 	 structure.Dict,
	}
}

func GetFromPBStruct(structure *PBStruct) *datastruct.Struct {
	array := make([]int, len(structure.Array))
	for idx, item := range structure.Array {
		array[idx] = int(item)
	}
	return &datastruct.Struct{
		Int:    int(structure.Int),
		Double: structure.Double,
		String: structure.String_,
		//Array:  structure.Array,
		Array:  array,
		//Dict:   structure.Dict,
	}
}
