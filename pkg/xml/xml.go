package xml

import (
	"encoding/xml"
	"gitlab.com/Sonichka1311/soa-hw2/pkg/datastruct"
	"log"
)

func Serialize(structure *datastruct.Struct) []byte {
	res, err := xml.Marshal(structure)
	if err != nil {
		log.Fatalf("Error while serialize XML: %s", err.Error())
	}
	return res
}

func Deserialize(data []byte, to *datastruct.Struct) {
	err := xml.Unmarshal(data, &to)
	if err != nil {
		log.Fatalf("Error while deserialize XML: %s", err.Error())
	}
}
