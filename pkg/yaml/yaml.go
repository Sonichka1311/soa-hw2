package yaml

import (
	"gitlab.com/Sonichka1311/soa-hw2/pkg/datastruct"
	"gopkg.in/yaml.v2"
	"log"
)

func Serialize(structure *datastruct.Struct) []byte {
	res, err := yaml.Marshal(structure)
	if err != nil {
		log.Fatalf("Error while serialize yaml: %s", err.Error())
	}
	return res
}

func Deserialize(data []byte, to *datastruct.Struct) {
	err := yaml.Unmarshal(data, &to)
	if err != nil {
		log.Fatalf("Error while deserialize yaml: %s", err.Error())
	}
}

